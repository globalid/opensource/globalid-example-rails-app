# frozen_string_literal: true

class User < ApplicationRecord
  validates :globalid_uuid,
            presence: true,
            uniqueness: { case_sensitive: false }

  devise :registerable, :rememberable, :validatable
  devise :database_authenticatable, authentication_keys: %i[globalid_uuid]
  devise :omniauthable, omniauth_providers: Devise.omniauth_providers

  def self.from_omniauth(auth)
    user = find_or_initialize_by(globalid_uuid: auth&.uid)
    return user if user.persisted?

    user.password = Devise.friendly_token[0, 20]

    user.username = auth&.info&.nickname
    user.name = auth&.info&.name

    return user if user.save
  end

  def display_name
    name || username || "anonymous"
  end

  # Disable Devise email validation
  def email_required?
    false
  end

  # Disable Devise email validation
  def will_save_change_to_email?
    false
  end
end
