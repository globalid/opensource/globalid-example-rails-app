class HomeController < ApplicationController
  def index
    redirect_to account_path if signed_in?
  end
end
