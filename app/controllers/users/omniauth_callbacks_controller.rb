# frozen_string_literal: true

module Users
  class OmniauthCallbacksController < Devise::OmniauthCallbacksController
    def globalid
      auth = request.env["omniauth.auth"]
      @user = User.from_omniauth(auth)

      if @user.present?
        flash[:notice] = "Successfully authenticated with your globaliD account." if is_navigational_format?
        sign_in_and_redirect(@user)
      else
        flash[:alert] = "Sorry, we couldn't authenticate you with globaliD. Please contact support."
        redirect_to root_url
      end
    end
  end
end
