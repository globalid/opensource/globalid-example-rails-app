# frozen_string_literal: true

module Users
  class SessionsController < Devise::SessionsController
    def new
      redirect_to user_globalid_omniauth_authorize_path
    end
  end
end
