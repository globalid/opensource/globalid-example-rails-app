class ApplicationController < ActionController::Base
  def user_root_path
    signed_in? ? account_path : root_path
  end

  def after_sign_in_path_for(resource)
    stored_location_for(resource) || user_root_path
  end

  helper_method :user_root_path
end
