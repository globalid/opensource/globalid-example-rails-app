module ApplicationHelper
  def plus_sign
    "&#43;".html_safe
  end

  def git_repo_path
    "https://gitlab.com/globalid/opensource/globalid-example-rails-app"
  end
end
