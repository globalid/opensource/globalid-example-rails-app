# frozen_string_literal: true

Rails.application.routes.draw do
  resource :account

  devise_for :users,
             controllers: {
               omniauth_callbacks: "users/omniauth_callbacks",
               sessions: "users/sessions",
               registrations: "users/registrations",
             }

  root to: "home#index"
end
