[GlobaliD](https://global.id) Example Rails app
=========

This app provides examples for integrating with globaliD. It uses Devise's [OmniAuth integration][omniauth-overview] and the [omniauth-globalid][] gem to enable users to authenticate using globaliD.

It requires that users authenticate with globaliD (it blocks Devise's email & password authentication).

[omniauth-overview]: https://github.com/plataformatec/devise/wiki/OmniAuth:-Overview
[omniauth-globalid]: https://rubygems.org/gems/omniauth-globalid

### [Live version of this app](https://globalid-example-rails.herokuapp.com)

Contents:

- [Dependencies](#dependencies)
- [Walkthrough](#walkthrough)
  - [Basic app setup](#basic-app-setup)
  - [Add globaliD omniauth](#add-globalid-omniauth)
  - [Handle globaliD callbacks](#handle-globalid-callbacks)
  - [Restrict authentication to globaliD](#restrict-authentication-to-globalid)
- [Continuous Integration & Deployment](#continuous-integration---deployment)


Dependencies
------------

- Ruby
- Rails
- Devise
- omniauth-globalid

Walkthrough
-----------

### Basic app setup

Initialize a new Rails app

```shell
rails new globalid-example-rails --database=postgresql --skip-test
```

Add Devise and omniauth-globalid gems to your Gemfile and `bundle install`

```ruby
# Gemfile

gem "devise"
gem "omniauth-globalid"
```

Install and configure Devise (see the [Devise documentation][devise-docs] for details).

[devise-docs]: https://github.com/plataformatec/devise#getting-started

Add a globalid_uuid, username, and name to the User model. For this app, we use a user's globaliD uuid as their authentication key rather than Devise's default of email.

You can examine this app's [user creation migration](db/migrate/20190911130005_create_users.rb), or add the required attributes with:

```shell
rails g migration add_globalid_uuid_name_and_username_to_users globalid_uuid:string:uniq name:string username:string
```

```ruby
# config/initializers/devise.rb

config.authentication_keys = [:globalid_uuid]
config.case_insensitive_keys = [:globalid_uuid]
config.strip_whitespace_keys = [:globalid_uuid]
```

```ruby
# app/models/user.rb

# Disable Devise email validation
def email_required?
  false
end

def will_save_change_to_email?
  false
end
```

Create and migrate the database.

```shell
rails db:create db:migrate
```

### Add globaliD omniauth

First create a globaliD developer app [globaliD developer dashboard][gid-newapp] and note the client and secret.

Copy [.env.sample](.env.sample) to `.env` and add the values from the dashboard to the env file, in `GLOBALID_CLIENT_ID` and `GLOBALID_CLIENT_SECRET`

You will need to create a private key pair, with an empty password, for PII Sharing and to be able to use the WebClient. We recommend using the `GLOBALID_CLIENT_ID` as the filename. To upload the public key, it must be in .pem format. Create a key and print out the public key in .pem format with:

```bash
GLOBALID_CLIENT_ID=<YOUR globaliD client id>
ssh-keygen -t rsa -b 4096  -f ~/.ssh/${GLOBALID_CLIENT_ID}_id_rsa
ssh-keygen -f ~/.ssh/${GLOBALID_CLIENT_ID}_id_rsa.pub -m 'PEM' -e
```

[gid-newapp]: https://developer.global.id/app/developer/new
[dotenv]: https://github.com/bkeepers/dotenv


Add the globaliD omniauth strategy to the app's Devise configuration.

```ruby
# config/initializers/devise.rb

config.omniauth :globalid, ENV.fetch("GLOBALID_CLIENT_ID"), ENV.fetch("GLOBALID_CLIENT_SECRET")
```

#### 1. Local development

To test the globaliD integration running a local server in development mode, you'll need SSL - so use a tool like [ngrok](https://ngrok.com) (`brew cask install ngrok`) to expose a public URL to your localhost

Start ngrok, exposing the port your Rails app is running on:

```shell
ngrok http 3000
# Forwarding https://0ea4aaf2.ngrok.io -> http://localhost:3000
```

On the globaliD developer dashboard, set the "Whitelisted Redirect URLs" of your [globaliD app][gid-dashboard] to your ngrok URL.

Expose the generated subdomain as an environment variable to allow your Rails app to accept requests forwarded through it:

[gid-dashboard]: https://developer.global.id/

Add just the subdomain of the url to `.env`

```shell
NGROK_SUBDOMAIN: 0e3aaaf2
```

```ruby
# config/environments/development.rb

config.hosts << "#{ENV['NGROK_SUBDOMAIN']}.ngrok.io" if ENV.key?("NGROK_SUBDOMAIN")
```

#### 2. Views

Add a link to log in with globaliD somewhere in your views.

```ruby
link_to "log in with globaliD", user_globalid_omniauth_authorize_path
```

when a user clicks on this link, they'll be redirected to globaliD to authenticate with the globaliD app on their smart phone. When they approve the auth request, they'll be redirected back to the Whitelisted Redirect URL set in your [globaliD app settings][gid-dashboard] - here, `[APP_HOST]/users/auth/globalid/callback`.

### Handle globaliD callbacks

```ruby
# app/controllers/users/omniauth_callbacks_controller.rb

class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def globalid
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.present?
      flash[:notice] = "Successfully authenticated with your globaliD account." if is_navigational_format?
      sign_in_and_redirect(@user)
    else
      flash[:alert] = "Sorry, we couldn't authenticate you with globaliD. Please contact support."
      redirect_to root_url
    end
  end
```

```ruby
# app/models/user.rb

def self.from_omniauth(auth)
  user = find_or_initialize_by(provider: auth&.provider, globalid_uuid: auth&.uid)
  return user if user.persisted?

  user.password = Devise.friendly_token[0, 20]

  user.username = auth&.info&.nickname
  user.name = auth&.info&.name

  return user if user.save
end
```

```ruby
# config/routes.rb

devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }
```

### Restrict authentication to globaliD

To restrict authentication to globaliD, we can override the default Devise controller actions for handling sign-in and sign-up requests.

```shell
rails g devise:controllers users
```

```ruby
# app/controllers/users/registrations_controller.rb

class RegistrationsController < Devise::RegistrationsController
  def new
    redirect_to user_globalid_omniauth_authorize_path
  end
end
```

```ruby
# app/controllers/users/sessions_controller.rb

class SessionsController < Devise::SessionsController
  def new
    redirect_to user_globalid_omniauth_authorize_path
  end
end
```

Additionally, we'll need to update the routes to point to our subclassed controllers:

```ruby
# config/routes.rb

devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks", sessions: "users/sessions", registrations: "users/registrations" }
```

Continuous Integration & Deployment
-----------------------------------

To set up an GitLab CI Pipeline, add `.gitlab-ci.yml` to your project root. You can use GitLab Pipelines to run your app's tests on GitLab and automate deployments.

This example app depends on Ruby 2.6.4 and PostgreSQL:

```yml
# .gitlab-ci.yml

image: ruby:2.6.4

services:
  - postgres:latest

variables:
  DB_HOST: postgres
  DB_USER: postgres
```
<sup>[[source](https://gitlab.com/globalid/opensource/globalid-example-rails-app/blob/b7d9938e/.gitlab-ci.yml#L1-8)]</sup>

We'll want to make sure the app's database configuration is updated to use the correct `host` and `user`:

```yml
# config/database.yml

default: &default
  adapter: postgresql
  encoding: unicode
  # For details on connection pooling, see Rails configuration guide
  # https://guides.rubyonrails.org/configuring.html#database-pooling
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  host: <%= ENV["DB_HOST"] %>
  user: <%= ENV.fetch("DB_USER", "postgres") %>
```
<sup>[[source](https://gitlab.com/globalid/opensource/globalid-example-rails-app/blob/b7d9938e/config/database.yml#L17-24)]</sup>

This pipeline will have two jobs, "test" and "staging", the former running in the test stage the latter in the deploy stage.

```yml
# .gitlab-ci.yml

test:
  variables:
    GLOBALID_CLIENT_ID: test
    GLOBALID_CLIENT_SECRET: test
    GLOBALID_CLIENT_ACRC_ID: test
  stage: test
  script:
    - apt-get update -qy
    - apt-get install -y nodejs
    - gem install bundler:2.0.2
    - bundle install --path /cache
    - bundle exec rake db:create RAILS_ENV=test
    - bundle exec rspec
```
<sup>[[source](https://gitlab.com/globalid/opensource/globalid-example-rails-app/blob/b7d9938e/.gitlab-ci.yml#L10-22)]</sup>

Ensure the `HEROKU_STAGING_API_KEY` environment variable is set by navigating to [Your Project] > Settings > CI / CD > Variables and entering your Heroku API key, which you can find on your [Heroku Dashboard](https://dashboard.heroku.com/account).

```yml
# .gitlab-ci.yml

staging:
  stage: deploy
  script:
    - gem install dpl
    - dpl --provider=heroku --app=globalid-example-rails --api-key=$HEROKU_STAGING_API_KEY
  only:
  - master
```
<sup>[[source](https://gitlab.com/globalid/opensource/globalid-example-rails-app/blob/b7d9938e/.gitlab-ci.yml#L24-30)]</sup>
