class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      # Database authenticatable
      t.string :globalid_uuid, null: false

      t.string :username
      t.string :name
      t.string :encrypted_password, null: false, default: ""

      # Rememberable
      t.datetime :remember_created_at

      t.timestamps
    end

    add_index :users, :globalid_uuid, unique: true
  end
end
