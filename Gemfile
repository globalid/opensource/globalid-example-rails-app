# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }
git_source(:gitlab) { |repo| "https://gitlab.com/#{repo}.git" }

ruby "2.6.5"

gem "bootsnap", ">= 1.4.2", require: false
gem "bootstrap", "~> 4.3.1"
gem "devise"
gem "jbuilder", "~> 2.7"
gem "jquery-rails"
gem "omniauth-globalid", ">= 0.0.7"
gem "pg", ">= 0.18", "< 2.0"
gem "puma", "~> 3.11"
gem "rails", "~> 6.0.0"
gem "sass-rails", "~> 5"

group :development do
  gem "listen", ">= 3.0.5", "< 3.2"
  gem "rufo"
  gem "spring"
  gem "spring-commands-rspec"
  gem "spring-watcher-listen", "~> 2.0.0"
  gem "web-console", ">= 3.3.0"
end

group :development, :test do
  gem "dotenv-rails"
  gem "factory_bot_rails"
  gem "jazz_fingers"
  gem "pry-byebug"
  gem "pry-rails"
  gem "rspec-rails"
  gem "vcr"
  gem "webmock"
  gem "guard" # file watcher
  gem "guard-rspec", require: false # Automatically rerun specs on changes
  gem "guard-rubocop", require: false
end

group :test do
  gem "capybara"
  gem "spicy-proton"
end
