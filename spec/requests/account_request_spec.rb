# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Account", type: :request do
  describe "GET /account" do
    context "when logged in" do
      let(:user) { FactoryBot.create(:user) }
      before { sign_in(user) }
      it "responds with 200" do
        user = FactoryBot.create(:user)
        get account_path
        expect(response).to have_http_status(200)
      end
    end

    it "redirects to the login page" do
      get account_path
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
