# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    username { Spicy::Proton.pair(" ") }
    password { "password" }
    globalid_uuid { SecureRandom.uuid }
  end
end
